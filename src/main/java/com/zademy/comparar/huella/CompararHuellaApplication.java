package com.zademy.comparar.huella;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompararHuellaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompararHuellaApplication.class, args);
	}

}
