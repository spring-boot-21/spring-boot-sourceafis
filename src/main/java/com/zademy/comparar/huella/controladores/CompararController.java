package com.zademy.comparar.huella.controladores;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.machinezoo.sourceafis.FingerprintImage;
import com.machinezoo.sourceafis.FingerprintImageOptions;
import com.machinezoo.sourceafis.FingerprintMatcher;
import com.machinezoo.sourceafis.FingerprintTemplate;

/**
 * The Class CompararController.
 */
@RestController
public class CompararController {

	/**
	 * Comparar.
	 *
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@GetMapping("/comparar")
	public String comparar() throws IOException {

		return compararHuellaImagen();
	}

	/**
	 * Comparar huella imagen.
	 *
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private String compararHuellaImagen() throws IOException {

		double dpi = 500;

		FingerprintTemplate huella1 = new FingerprintTemplate(new FingerprintImage(
				Files.readAllBytes(Paths.get("src", "main", "resources", "static", "images/huella1.jpg")),
				new FingerprintImageOptions().dpi(dpi)));

		FingerprintTemplate huella2 = new FingerprintTemplate(new FingerprintImage(
				Files.readAllBytes(Paths.get("src", "main", "resources", "static", "images/huella1.jpg")),
				new FingerprintImageOptions().dpi(dpi)));

		String resultado = "";

		double puntuacion = new FingerprintMatcher(huella1).match(huella2);

		double umbral = 40;

		if (puntuacion >= umbral) {

			resultado = "Si coindicio la huella con una puntuación de " + Math.round(puntuacion) + " >= 40";

		} else {

			resultado = "No coindicio la huella con una puntuación de " + Math.round(puntuacion) + " < 40";
		}

		return resultado;

	}

}
