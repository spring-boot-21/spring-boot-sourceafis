## Pre-requisitos
- Spring Boot 2.6.11
- Spring 5.3.22
- Tomcat embed 9.0.65
- Maven 3.6.6
- Java 8

## Construir proyecto
```
mvn clean install
```
